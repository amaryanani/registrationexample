import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

class JDBC {
    Connection connector = null;

    Connection getConnector() {
        if (connector == null) {
            openConnection();
            return connector;
        }
        return connector;
    }

    void openConnection() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            System.out.println("Congrats - Seems your MySQL JDBC Driver Registered!");
        } catch (ClassNotFoundException e) {
            System.out.println("Sorry, couldn't found JDBC driver. Make sure you have added JDBC Maven Dependency Correctly");
            e.printStackTrace();
        }

        try {
            connector = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb", "root", "root");
            if (connector != null) {
                System.out.println("Connection Successful! Enjoy. Now it's time to push data");
            } else {
                System.out.println("Failed to make connection!");
            }
        } catch (SQLException e) {
            System.out.println("MySQL Connection Failed!");
            e.printStackTrace();
        }
    }
}
