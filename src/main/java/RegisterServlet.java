import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class RegisterServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        if (name.isEmpty() || email.isEmpty() || password.isEmpty()) {
            PrintWriter printWriter = response.getWriter();
            RequestDispatcher dispatcher = request.getRequestDispatcher("registration.jsp");
            printWriter.println("<font color=red>Please fill all the fields</font>");
            dispatcher.include(request, response);
        } else {
            UserRegister.addDataToDB(name, email, password);
            RequestDispatcher dispatcher = request.getRequestDispatcher("src/main/webapp/welcome.jsp");
            dispatcher.forward(request, response);
        }
    }
}
