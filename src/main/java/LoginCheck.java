import java.sql.PreparedStatement;
import java.sql.ResultSet;

class LoginCheck {

    static boolean checkUser(String name, String password) {
        boolean st = false;
        try {
            JDBC jdbc = new JDBC();
            jdbc.openConnection();
            PreparedStatement preparedStatement = jdbc.connector.
                    prepareStatement("select * from data where name=? and password=?");
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, password);

            ResultSet rs = preparedStatement.executeQuery();
            st = rs.next();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return st;
    }
}
