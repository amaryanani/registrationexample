import java.sql.PreparedStatement;
import java.sql.SQLException;

class UserRegister {

    static void addDataToDB(String name, String email, String password) {
        try {
            String insertQueryStatement = "INSERT  INTO  data  VALUES  (?,?,?)";
            JDBC JDBC = new JDBC();
            JDBC.openConnection();
            PreparedStatement preparedStatement = JDBC.getConnector().prepareStatement(insertQueryStatement);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, email);
            preparedStatement.setString(3, password);

            preparedStatement.executeUpdate();
            System.out.println(name + " added successfully");
            preparedStatement.close();
            JDBC.getConnector().close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
